public class Servidor extends Thread 
{

	private Buffer buffer;
	private int id;

	public Servidor(Buffer pBuffer, int i) 
	{
		buffer = pBuffer;
		id = i;
	}

	@Override
	public void run() 
	{
		while(true)
		{
			atenderMensaje();
		}
	}

	public synchronized void atenderMensaje() 
	{
		//RESPONDER CONSULTA (responder	consiste en	 incrementar	 el	 valor	 del	 mensaje	 y	 avisarle	 al	siguiente cliente	 que	 puede	 continuar)

		Mensaje mensaje = buffer.retirar();
		if(mensaje == null && !buffer.hayClientes())
		{
			System.out.println("FIN:\nAqu� termin� el servidor " + id+ "\n");
			stop();
		}
		
		mensaje.generarRespuesta();
		System.out.println("PETICI�N ATENDIDA:\nSe pudo retirar el mensaje " + mensaje.darConsulta() + " enviado por el cliente "+mensaje.darClienteQueEnvio()+" y se gener� la respuesta "+ mensaje.darRespuesta());

	}

}