import java.io.FileInputStream;
import java.util.Properties;

public class Main 
{

	private static final int TAMANIO_BUFFER = 50;
	private static String rutaArchivo;
	private static int numClientes;
	private static int numServidores;
	private static int[] numConsultasClientes;
	private static Buffer buffer;

	public static void main(String[] args) 
	{
		rutaArchivo = "./data/archivo.txt";
		System.out.println("-------------- SIMULADOR SISTEMA GDTS ----------------");
		System.out.println();
		try
		{
			Properties datos = new Properties( );
			FileInputStream in = new FileInputStream( rutaArchivo );
			datos.load( in );
			in.close( );
			
			//LEER LOS DATOS NECESARIOS PARA LA INICIALIZACIÓN
			numClientes = Integer.parseInt(datos.getProperty("numClientes"));
			numServidores = Integer.parseInt(datos.getProperty("numServidores"));
			numConsultasClientes = new int[numClientes];
			for (int i = 0; i < numConsultasClientes.length; i++) 
			{
				numConsultasClientes[i] = Integer.parseInt(datos.getProperty("numConsultasCliente"+(i+1)));
			}
			
			System.out.println("Se cargó la información del archivo");
			System.out.println("Numero de clientes = " + numClientes);
			System.out.println("Numero de servidores = "+numServidores);
			System.out.println("Numero de consultas por cliente:");
			for (int i = 0; i < numConsultasClientes.length; i++) 
			{
				System.out.println("Cliente "+(i+1)+" = "+numConsultasClientes[i]);
			}
			System.out.println();
			
			buffer = new Buffer(TAMANIO_BUFFER, numClientes);
			
			for (int i = 0; i < numClientes; i++) 
			{
				new Cliente(i+1,numConsultasClientes[i], buffer).start();
			}
			
			for (int i = 0; i < numServidores; i++) 
			{
				new Servidor(buffer, i+1).start();
			}
			
		}
		catch( Exception e )
		{
			System.out.println("Error");
		}
	}

}