
public class Buffer 
{

	private Mensaje[] buffer;
	private int tamanio;
	private static int cantidadElementosActuales;
	private static int contadorPosicion;
	private int cantidadClientes;
	private boolean todosTerminaron;

	public Buffer(int pTamanio, int numClientes) 
	{
		tamanio = pTamanio;
		buffer = new Mensaje[tamanio];
		cantidadElementosActuales = 0;
		contadorPosicion = 0;
		cantidadClientes = numClientes;
		todosTerminaron = false;
	}

	public synchronized void almacenar(Mensaje mns)
	{
		buffer[contadorPosicion] = mns;
		contadorPosicion++;
		cantidadElementosActuales++;
		synchronized(this){notify();}
	}

	public synchronized Mensaje retirar()
	{
		boolean servidorEstaBloqueado = false;
		Mensaje result = null;
		try 
		{
			while(estaVacio())
			{
				System.out.println("BLOQUEO:\nServidor bloqueado porque el buffer est� vac�o");
				servidorEstaBloqueado = true;
				if(!todosTerminaron)
				{
					synchronized(this){wait();}	
				}
				
				if(!hayClientes())
				{
					break;
				}
			} 
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		if(!todosTerminaron)
		{
			if(servidorEstaBloqueado)
			{
				System.out.println("DESBLOQUEO:\nDesbloqueo del servidor, ya no est� vac�o el buffer");
				servidorEstaBloqueado = false;
			}

			cantidadElementosActuales--;
			contadorPosicion--;
			result = buffer[contadorPosicion];
		}

		return result;
	}

	public boolean estaLleno() 
	{
		boolean res = false;
		if(cantidadElementosActuales == tamanio)
			res = true;

		return res;
	}

	public boolean estaVacio() 
	{
		boolean res = false;
		if(cantidadElementosActuales == 0)
		{
			res = true;
		}

		return res;
	}

	public synchronized void avisarQueClienteSeRetira(int id) 
	{
		cantidadClientes--;
		synchronized(System.class)
		{
			System.out.println("\n**********************************\n"+"El cliente " + id + " ya termin�\n"+"Faltan " + cantidadClientes + " por terminar");
			if(cantidadClientes == 0)
			{
				System.out.println("TODOS LOS CLIENTES HAN TERMINADO");
				todosTerminaron = true;
				synchronized(this){notifyAll();}
			}

			System.out.println("**********************************");
			System.out.println();
		}
	}

	public boolean hayClientes() 
	{
		return !todosTerminaron;
	}

}