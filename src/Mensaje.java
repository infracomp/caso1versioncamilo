public class Mensaje 
{

	private int consulta;
	private Integer respuesta;
	private int idCliente;

	public Mensaje(int pCont, int pIdCliente) 
	{
		consulta = pCont;
		respuesta = null;
		idCliente = pIdCliente;
	}

	public synchronized int darRespuesta() 
	{
		boolean clienteBloqueado = false;
		try 
		{
			while(respuesta == null)
			{
				System.out.println("BLOQUEO:\nCliente " + idCliente + " bloqueado a la espera de una respuesta");
				clienteBloqueado = true;
				synchronized(this){wait();}
			}
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		if(clienteBloqueado == true)
		{
			System.out.println("DESBLOQUEO:\nDesbloqueo del cliente " + idCliente+ " porque ya hay una respuesta");
			clienteBloqueado = false;
		}

		return respuesta;
	}

	public void generarRespuesta() 
	{
		respuesta = consulta+1;
		synchronized(this){notify();}
	}

	public int darConsulta() 
	{
		return consulta;
	}

	public int darClienteQueEnvio() 
	{
		return idCliente;
	}

}