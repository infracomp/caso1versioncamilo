public class Cliente extends Thread 
{

	private int numConsultas;
	private Buffer buffer;
	private int id;
	
	public Cliente(int pId, int numConsultasPorCliente, Buffer pBuffer) 
	{
		id = pId;
		numConsultas = numConsultasPorCliente;
		buffer = pBuffer;
	}
	
	@Override
	public void run() 
	{
		for (int i = 0; i < numConsultas; i++) 
		{
			Mensaje mensaje = new Mensaje((int)(Math.random()*100), id);
			enviarMensaje(mensaje);
		}
		// AVISAR AL BUFFER QUE SE RETIRA
		buffer.avisarQueClienteSeRetira(id);
	}

	public synchronized void enviarMensaje(Mensaje mensaje) 
	{
		// PROCESO DE ENVIAR MENSAJE AL BUFFER
		
		while(buffer.estaLleno())
		{
			System.out.println("ESPERA ACTIVA:\nNo es posible enviar el mensaje porque el buffer esta lleno");
			yield();
		}
		
		buffer.almacenar(mensaje);
		System.out.println("CONSULTA ENVIADA:\nSe envio correctamente el mensaje " + mensaje.darConsulta() +" por el cliente " + id);
		mensaje.darRespuesta();
	}


}